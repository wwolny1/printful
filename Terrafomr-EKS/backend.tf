terraform {
  backend "s3" {
    access_key = "ACCESS_KEY"
    secret_key = "SECRET_KEY"
    region     =  var.region
    bucket     = "eks-terraform-state-s3" 
    key        = "terraform.tfstate"
  }
}
