data "aws_eks_cluster" "cluster" {
    name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
    name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks"{
    source = "terraform-aws-modules/eks/aws"
    cluster_name = local.cluster_name
    cluster_version = "1.21"
    subnets = module.vpc.private_subnets
tags = {
        Name = "EKS-Cluster"
    }
vpc_id = module.vpc.vpc_id
    workers_group_defaults = {
        root_volume_type = "gp2"
        root_volume_size = "32"
    }
    
cluster_endpoint_private_access = true
cluster_endpoint_public_access  = true


worker_groups = [
        {
            name = "Worker-Group"
            instance_type = "t2.micro"
            asg_desired_capacity = 3
            additional_security_group_ids = [aws_security_group.worker_group_mgmt.id]
        },
    ]
}