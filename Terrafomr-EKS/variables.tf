variable "region" {
  description = "The region to host the cluster in"
  default     = "eu-central-1"
}