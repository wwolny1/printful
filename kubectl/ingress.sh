#!/bin/bash
set -e

# Update these before starting
export CdnClusterName=EKS-Cluster
export CdnAwsAccount=<AWS accoun nr>
export CdnAwsRegion=eu-central-1



echo "create IAM OIDC provider"
eksctl utils associate-iam-oidc-provider --cluster ${CdnClusterName} --approve

echo "download IAM policy for the LoadBalancerController"
curl -s -o iam_policy.json https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.1.3/docs/install/iam_policy.json

echo "create IAM policy"
aws iam create-policy --policy-name AWSLoadBalancerControllerIAMPolicy --policy-document file://iam_policy.json

rm -f iam_policy.json

echo "create IAM service account"
eksctl create iamserviceaccount \
  --cluster=${CdnClusterName} \
  --namespace=kube-system \
  --name=aws-load-balancer-controller \
  --attach-policy-arn=arn:aws:iam::${CdnAwsAccount}:policy/AWSLoadBalancerControllerIAMPolicy  \
  --override-existing-serviceaccounts \
  --approve

echo "install LoadBalancerController CRDs helm chart"
kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller//crds?ref=master"
helm repo add eks https://aws.github.io/eks-charts

echo "install LoadBalancerController helm chart"
helm upgrade -i aws-load-balancer-controller eks/aws-load-balancer-controller \
  --set clusterName=${CdnClusterName} \
  --set serviceAccount.create=false \
  --set serviceAccount.name=aws-load-balancer-controller \
  --set region=${CdnAwsRegion} \
  -n kube-system

kubectl get deployment -n kube-system aws-load-balancer-controller